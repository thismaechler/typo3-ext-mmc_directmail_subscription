<?php
defined('TYPO3') || die();

(function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'MmcDirectmailSubscription',
        'Subscr',
        [\MMC\MmcDirectmailSubscription\Controller\SubscribeController::class => 'register, registerConfirm, cancel, cancelConfirm'],
        // non-cacheable actions
        [\MMC\MmcDirectmailSubscription\Controller\SubscribeController::class => 'register, registerConfirm, cancel, cancelConfirm']
    );
})();
