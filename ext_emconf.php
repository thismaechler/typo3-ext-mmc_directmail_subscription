<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'MMC directmail subscription',
    'description' => 'Custom direct-mail subscription plugin',
    'category' => 'plugin',
    'author' => 'Matthias Mächler',
    'author_email' => 'maechler@mm-computing.ch',
    'state' => 'stable',
    'uploadfolder' => false,
    'version' => '3.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'tt_address' => '7.1.0-7.99.99',
            'direct_mail' => '9.2.2-9.99.99'
        ],
        'conflicts' => [],
        'suggests' => []
    ]
];
