var mmcDmlSubscr = {

	nameFieldsAllowedCharsRegEx: /./,

	validateNameFieldInput: function(e){
		var c = String.fromCharCode(e.which);
		if( !c.match(mmcDmlSubscr.nameFieldsAllowedCharsRegEx) )
			return false;
		return true;
	},

	submitForm: function(){
		if( mmcDmlSubscr.validateForm( '.tx-mmc-directmail-subscription .mmc-dms-form' ) ){
			return true;
		}
		return false;
	},

	validateForm: function( formSelector ){
		var formValid = true;
		$(formSelector + '.form-row').removeClass('error');
		$( formSelector + ' .form-row .validate').each(function(){
			var fieldValid = true;
			// required check
			if( $(this).hasClass('required') && ! $(this).val() ){
				fieldValid = false;
			}
			// email format check
			if ( $(this).hasClass('email') && ! $(this).val().match(/^(\S+@\S+\.\S+|)$/) ){
				fieldValid = false;
			}
			// field repeat check
			if( $(this).hasClass('repeat') && ( $(this).val() != $('#'+this.id+'-repeat').val() ) ){
			  fieldValid = false;
			  $('#'+this.id+'-repeat').val('');
			}
			// invalidate form, set field focus on first error field
			if( !fieldValid ){
				if( formValid ){
					$(this).focus();
				  formValid = false;
				}
				$(this).closest('.form-row').addClass('error');
				$('#'+this.id+'-repeat').closest('tr').addClass('error');
			}
		});
		return formValid;
	}



}
