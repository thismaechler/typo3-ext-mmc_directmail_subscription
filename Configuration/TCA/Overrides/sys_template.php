<?php
defined('TYPO3') || die();

(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('mmc_directmail_subscription', 'Configuration/TypoScript', 'MMC directmail subscription');
})();
