<?php
defined('TYPO3') || die();

(function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'MmcDirectmailSubscription',
        'Subscr',
        'MMC directmail subscription'
    );
})();
