<?php

declare(strict_types = 1);

return [
  \MMC\MmcDirectmailSubscription\Domain\Model\Address::class => [
    'tableName' => 'tt_address'
  ]
];
