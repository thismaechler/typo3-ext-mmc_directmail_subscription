<?php
namespace MMC\MmcDirectmailSubscription\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Address
 */
class AddressRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
        
    public function initializeObject()
    {
        $this->defaultQuerySettings = $this->createQuery()->getQuerySettings()
            ->setEnableFieldsToBeIgnored(['disabled'])
            ->setRespectSysLanguage(false)
            ->setLanguageOverlayMode(false)
            ->setIgnoreEnableFields( true );
        $this->setDefaultQuerySettings( $this->defaultQuerySettings );
    }

    public function setFindDisabled( $findDisabled )
    {
        $this->defaultQuerySettings->setIgnoreEnableFields( $findDisabled );
        $this->setDefaultQuerySettings( $this->defaultQuerySettings );
    }

    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->matching( $query->equals('uid', $uid) );
        $result = $query->execute();
        return $result->getFirst();
    }
    
    
    /**
    * removeAllOthersWithIdenticalEmail
    * removes all records with email identical to $address, except $address itself
    *
    * @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address 
    * @return void
    */
    public function removeAllOthersWithIdenticalEmail( $address )
    {
        $addr = $this->findByEmail( $address->getEmail() );
        foreach( $addr as $a ){
            if( $a->getUid() != $address->getUid() )
                $this->remove( $a );
        }
    }
    
    public function persistAll()
    {
        $this->persistenceManager->persistAll();
    }
    
}