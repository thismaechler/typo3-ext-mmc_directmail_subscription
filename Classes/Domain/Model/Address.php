<?php
namespace MMC\MmcDirectmailSubscription\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Address for tt_address
 */
class Address extends \FriendsOfTYPO3\TtAddress\Domain\Model\Address
{

    /**
     * @var string
     */
    protected $emailVerificationCode;
    
    /**
     * @var boolean
     */
    protected $moduleSysDmailHtml;


    /**
     * Returns the emailVerificationCode
     *
     * @return string $emailVerificationCode
     */
    public function getEmailVerificationCode() 
    {
        return $this->emailVerificationCode;
    }

    /**
     * Sets the emailVerificationCode
     *
     * @param string $emailVerificationCode
     * @return void
     */
    public function setEmailVerificationCode($emailVerificationCode) 
    {
        $this->emailVerificationCode = $emailVerificationCode;
    }
    
    /**
     * Returns the moduleSysDmailHtml
     *
     * @return boolean $moduleSysDmailHtml
     */
    public function getModuleSysDmailHtml() 
    {
        return $this->moduleSysDmailHtml;
    }

    /**
     * Sets the moduleSysDmailHtml
     *
     * @param boolean $moduleSysDmailHtml
     * @return void
     */
    public function setModuleSysDmailHtml($moduleSysDmailHtml) 
    {
        $this->moduleSysDmailHtml = $moduleSysDmailHtml;
    }


}