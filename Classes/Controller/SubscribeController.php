<?php
namespace MMC\MmcDirectmailSubscription\Controller;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use Psr\Http\Message\ResponseInterface;

use MMC\MmcDirectmailSubscription\Domain\Repository\AddressRepository;
use MMC\MmcDirectmailSubscription\Domain\Model\Address;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* SubscribeController
*/
class SubscribeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{

    /**
     * addressRepository
     *
     * @var \MMC\MmcDirectmailSubscription\Domain\Repository\AddressRepository
     */
    protected $addressRepository = NULL;

    /**
     * currently loaded address, for use in child classes
     *
     * @var \MMC\MmcDirectmailSubscription\Domain\Model\Address
     */
    protected $address = NULL;


    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
    * action register
    * register address -> needs email verification to unhide record
    *
    * @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
    * @param string $captchaToken
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function registerAction( $address = NULL, $captchaToken = '' ): ResponseInterface 
    {  
        if( $address && $this->verifyCaptcha($captchaToken) && $this->validateNameFields($address) ){
            $address->setHidden( true );
            $address->setEmailVerificationCode( $this->generateVerificationString( 16 ) );
            $this->addressRepository->add( $address );
            $this->addressRepository->persistAll();
            $this->sendRegisterVerificationMail( $address );
            // re-set template
            $this->view->setTemplate('Subscribe/Register');
        } else {
            $address = NULL;
        }
        // set $this->address for use in child classes
        $this->address = $address;
        // assign to view, if NULL, view displays the register form, else a sent-mail notification
        $this->view->assign( 'address', $address );
        $this->view->assign( 'lang', $this->getCurrentLanguageIsoCode() );
        return $this->htmlResponseWithNoCacheHeaders($address);
    }

    /**
    * action registerConfirm
    *
    * @param string $addressUid
    * @param string $evc email verification code
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function registerConfirmAction( $addressUid = NULL, $evc = NULL ): ResponseInterface 
    {
        // verificate address..
        if( $address = $this->verificateAddress( $addressUid, $evc ) ){
            // remove records with identical email address..
            if( $this->settings['keepEmailAddressUnique'] ){
                $this->addressRepository->removeAllOthersWithIdenticalEmail( $address );
            }
            // confirmed, unhide & store..
            $address->setHidden( false );
            $this->addressRepository->update( $address );
            // persist
            $this->addressRepository->persistAll();
        }
        // set $this->address for use in child classes
        $this->address = $address;
        // assign to view, if NULL, view displays a fail message
        $this->view->assign( 'address', $address );
        return $this->htmlResponseWithNoCacheHeaders($address);
    }


    /**
    * action cancel
    *
    * @param string $email
    * @param string $captchaToken
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function cancelAction( $email = NULL, $captchaToken = ''  ): ResponseInterface  
    {
        // override default
        $this->addressRepository->setFindDisabled( false );
        $address = NULL;
        if( !$this->verifyCaptcha($captchaToken) ){
            $email = NULL; // CAPTCHA failed, just reset form
        }
        if(
            $email &&
            ($address = $this->addressRepository->findOneByEmail( $email ) )
        ){
            // address found, set email verification code and send mail..
            $address->setEmailVerificationCode( $this->generateVerificationString( 16 ) );
            $this->addressRepository->update( $address );
            $this->addressRepository->persistAll();
            $this->sendCancelVerificationMail( $address );
            // re-set template
            $this->view->setTemplate('Subscribe/Cancel');            
        }
        // set $this->address for use in child classes
        $this->address = $address;
        /* assign to view
         * email is NULL: view displays cancel form
         * email is not NULL, address is not NULL: view displays sent-mail confirmation
         * email is not NULL, address is NULL: view displays email-address not found message
         */
        $this->view->assign( 'email', $email );
        $this->view->assign( 'address', $address );
        return $this->htmlResponseWithNoCacheHeaders($address);
    }

    /**
    * action cancelConfirm
    *
    * @param string $addressUid
    * @param string $evc email verification code
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function cancelConfirmAction( $addressUid = NULL, $evc = NULL ): ResponseInterface  
    {
        // override default
        $this->addressRepository->setFindDisabled( false );
         // verificate address..
        if( $address = $this->verificateAddress( $addressUid, $evc ) ){
            // confirmed, remove address record
            if( $this->settings['removeAddress'] ) {
                $this->addressRepository->remove( $address );
            }else{
                $address->setHidden( true );
                $this->addressRepository->update( $address );
            }
            $this->addressRepository->persistAll();
        }
        // set $this->address for use in child classes
        $this->address = $address;
        // assign to view, if NULL, view displays a fail message
        $this->view->assign( 'address', $address );
        return $this->htmlResponseWithNoCacheHeaders($address);
    }

    /* ----------------------------- validation ------------------------------------------ */

    /**
    * validateNameFields
    *
    * @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
    * @return boolean
    */
    protected function validateNameFields( $address ) 
    {
        $regEx = $this->settings['nameFieldsAllowedCharsRegEx'];
        $maxLen = $this->settings['nameFieldsMaxChars'];
        $firstName = $address->getFirstName();
        $lastName = $address->getLastName();
        return (
            preg_match($regEx, $firstName) &&
            preg_match($regEx, $lastName) &&
            mb_strlen($firstName) <= $maxLen &&
            mb_strlen($lastName) <= $maxLen
        );
    }


    /* ----------------------------- CAPTCHA verification --------------------------------------- */

    /**
    * verifyCaptcha
    * Verify the submitted CAPTCHA-token according to the configured CAPTCHA-service
    *
    * @param string $captchaToken
    * @return bool true if CAPTCHA is valid
    */
    protected function verifyCaptcha( $captchaToken )
    {
        switch($this->settings['useCaptcha']){
            case 'none':
                return true;
            case 'frc':
                return $this->verifyFriendlyCaptcha($captchaToken);
            case 'grcv2_cb':
            case 'grcv2_iv':
                return $this->verifyGoogleReCaptchaV2($captchaToken);
            case 'grcv3':
                return $this->verifyGoogleReCaptchaV3($captchaToken);
        }
        return false;
    }

    /**
    * requestVerificationJSONObject
    * Send a POST request to $url and return result object
    *
    * @param string $url
    * @param array $post_params associative array key=>value
    * @return object
    */
    protected function requestVerificationJSONObject($url, $post_params)
    {
        $requestFactory = GeneralUtility::makeInstance(RequestFactory::class);
        $options = [
            // Additional headers for this specific request
          'headers' => ['Cache-Control' => 'no-cache'],
          // Additional options, see https://docs.guzzlephp.org/en/latest/request-options.html
          'allow_redirects' => false,
          'cookies' => false,
            'form_params' => $post_params
        ];
        $response = $requestFactory->request($url, 'POST', $options);
        // Get the content as a string on a successful request
        if ($response->getStatusCode() === 200) {
            $validationResponse = json_decode( $response->getBody()->getContents() );
            return $validationResponse;
        } else
        return NULL; // service not available
    }


    /* --- CAPTCHA service providers ---  */

    protected function verifyFriendlyCaptcha( $captchaToken )
    {
        $validationResponse = $this->requestVerificationJSONObject(
            'https://api.friendlycaptcha.com/api/v1/siteverify',
            [
                'solution' => $captchaToken,
                'secret' => $this->settings['captchaSecret'],
                'sitekey' => $this->settings['captchaSiteKey']
            ]
        );
        // Get the content as a string on a successful request
        if ( $validationResponse == NULL ){
            return true; // service not available, just trust the user
        }
        return $validationResponse->success;
    }

    protected function verifyGoogleReCaptchaV2( $captchaToken )
    {
        $validationResponse = $this->requestVerificationJSONObject(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'response' => $captchaToken,
                'secret' => $this->settings['captchaSecret']
            ]
        );
        // Get the content as a string on a successful request
        if ( $validationResponse == NULL ){
            return true; // service not available, just trust the user
        }
        return $validationResponse->success;
    }

    protected function verifyGoogleReCaptchaV3( $captchaToken )
    {
        $validationResponse = $this->requestVerificationJSONObject(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'response' => $captchaToken,
                'secret' => $this->settings['captchaSecret']
            ]
        );
        // Get the content as a string on a successful request
        if ( $validationResponse == NULL ){
            return true; // service not available, just trust the user
        }
        return (
            $validationResponse->success &&
            $validationResponse->score * 100 >= $this->settings['grc3ScoreTreshold']
        );
    }


    /* ----------------------------- email verification --------------------------------------- */

    /**
    * verificateAddress
    *
    * @param integer $addressUid
    * @param string $evc email verification code to check address record against
    * @return \MMC\MmcDirectmailSubscription\Domain\Model\Address $address NULL if invalid
    */
    protected function verificateAddress( $addressUid, $evc )
    {
        $address = NULL;
        if( $addressUid && $evc ){
            // avoid property override by arguments (necessary?), reload original record..
            if( $address = $this->addressRepository->findByUid( $addressUid ) ){
                // check verification code
                if( $address->getEmailVerificationCode() != $evc ){
                    $address = NULL;
                }
            }
        }
        return $address;
    }

    /**
    * generateVerificationString
    *
    * @param int $length of the random string
    * @return string $verificationString
    */
    protected function generateVerificationString( $length )
    {
        $chrs = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $chrsLen = strLen( $chrs );
        $vstr = '';
        for( $i = 0; $i < $length; $i++ )
            $vstr .= $chrs[rand(0, $chrsLen - 1)];
        return $vstr;
    }

    /**
    * sendRegisterVerificationMail
    *
    * @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
    * @return void
    */
    protected function sendRegisterVerificationMail( $address )
    {
        $this->sendVerificationMail(
            $this->translate('mail.register.subject'),
            $address,
            'Subscribe/RegisterMail.html'
        );
    }

    /**
    * sendCancelVerificationMail
    *
    * @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
    * @return void
    */
    protected function sendCancelVerificationMail( $address )
    {
        $this->sendVerificationMail(
            $this->translate('mail.cancel.subject'),
            $address,
            'Subscribe/CancelMail.html'
        );
    }

    /**
    * sendVerificationMail
    *
    * @param string $subject the email subject
    * @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
    * @param string $bodyTemplateFile the template file for the mail body
    * @return void
    */
    protected function sendVerificationMail( $subject, $address, $bodyTemplateFile )
    {
        // make mailer instance
        $mail = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        // setup email components
        $recipients = array( $address->getEmail() => $address->getName() );
        $from = array( $this->settings['fromEmail'] => $this->settings['fromName'] );
        // use controller view for rendering (to have context)
        $this->view->setTemplate($bodyTemplateFile);
        $this->view->assign( 'address', $address );
        $mailBody = $this->view->render();
        // send mail
        try{
            $mail->setFrom( $from )->setTo( $recipients )->setSubject( $subject );
              $mail->html($mailBody);
            $mail->send();
        } catch ( \Exception $e ){
            $this->addFlashMessage('error while sending mail: '.$e->getMessage() );
        }
        if(! $mail->isSent())
            $this->addFlashMessage('error while sending mail: mail could not be sent' );
    }


    /* ------------------------------------- Utility ---------------------------------------- */

    protected function translate( $key )
    {
        return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( $key, 'MmcDirectmailSubscription' );
    }


    /**
     * Set no-cache headers as soon as the view receives any data (address) that should not get cached somewhere on the road
     *
     * @return void
     */
    protected function htmlResponseWithNoCacheHeaders($address) 
    {
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'text/html; charset=utf-8')
            ->withBody($this->streamFactory->createStream((string)($this->view->render())));
        if ($address) {
            //set no-cache headers
            $response = $response
                // HTTP 1.1
                ->withHeader('Cache-Control', 'no-cache, must-revalidate')
                // HTTP 1.0
                ->withHeader('Pragma', 'no-cache')
                // robots
                ->withHeader('X-Robots-Tag', 'noindex, nofollow');
        }
        return $response;
    }

    private function getCurrentLanguageIsoCode()
    {
        return $this->request->getAttribute('language')->getTwoLetterIsoCode();
    }

}
